# ArchiTotem 1.31
This version adds support for de+fr localized WoW-clients to ArchiTotem-1.3 by Nahoom.

You do NOT need this version for the english wow-1.12 client, or if you already have a working config file.

Technicall, only the names of the totems in the config file must match the localization of your wow client. 
Then the original version will work.

Installation: obviously, copy the folder to the right destination, and delete an existing the config file
PATH_TO\WoW-1.12\WTF\Account\YOURACCONTNAME\SERVER\YOURSHAMAN\SavedVariables\ArchiTotem.lua).

Localization code is copied from the addon "TotemMenue" by ???.
Some "local" statements included in the lua code, as seen in Road-block's ArchiTotem git (sorry dude, cant live with your code formating).
All credits to the original authors!

Only ArchiTotem.lua (and Changelog.txt) are currently edited.
Original code is usually included as comment.
Minor changes to function  ArchiTotem_CastTotem()
